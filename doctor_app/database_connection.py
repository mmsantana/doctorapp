import psycopg2
from decouple import config


class DatabaseConnection:

    def __init__(self):
        self._conn = psycopg2.connect(
            database=config('DB_NAME'),
            user=config('DB_USER'),
            password=config('DB_PASSWORD'),
            host=config('DB_HOST'),
            port=config('DB_PORT'),
        )
        self._cur = self._conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.commit()
        self.connection.close()

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cur

    def commit(self):
        self.connection.commit()

    def execute(self, command):
        self.cursor.execute(command)

    def query_one(self, command):
        self.cursor.execute(command)
        return self.cursor.fetchone()

    def query_all(self, table_name):
        self.cursor.execute(f'SELECT * FROM {table_name}')
        return self.cursor.fetchall()

    def copy_expert(self, table_name, file):
        output_query = f'COPY (SELECT * FROM {table_name}) TO STDOUT WITH CSV HEADER'
        self.cursor.copy_expert(output_query, file)

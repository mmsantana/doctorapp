from psycopg2._psycopg import IntegrityError

from doctor_app.database_connection import DatabaseConnection
import datetime


class DoctorManagement:

    def __init__(self):
        self.database = DatabaseConnection
        self.field_options = "ID", "NOME", "CRM", "ESTADO", "CRIADO EM"
        self.field_translation = {
            "ID": "DOCTOR_ID",
            "NOME": "NAME",
            "CRM": "CRM",
            "ESTADO": "STATE"
        }

    def create_table(self):
        with self.database() as db:
            db.execute("""CREATE TABLE DOCTOR
            (DOCTOR_ID SERIAL,
            NAME VARCHAR NOT NULL,
            CRM VARCHAR UNIQUE NOT NULL,
            STATE VARCHAR NOT NULL,
            CREATED_AT TIMESTAMP NOT NULL);
            """)

    def insert_doctor(self):
        print('Insira as informações para cadastro do médico: ')
        name = input("Nome: ")
        crm = input("CRM: ")
        state = input("Estado: ")
        with self.database() as db:
            try:
                db.execute(f"""
                INSERT INTO DOCTOR (NAME,CRM,STATE,CREATED_AT) 
                VALUES ('{name}', '{crm}', '{state}', '{datetime.datetime.today()}')
                """)
            except IntegrityError:
                print("O CRM já existe!")
        return self.get_doctor(crm)

    def get_doctor(self, crm=None):
        if not crm:
            crm = input("CRM do médico: ")
        with self.database() as db:
            try:
                content = db.query_one(f"SELECT * FROM DOCTOR WHERE CRM = '{crm}'")
                content = self.get_types(content)
                template = "{0:15}|{1:15}|{2:15}|{3:15}|{4:15}"
                print(template.format(*self.field_options))
                print('-'*80)
                print(template.format(*content))
                return content
            except TypeError:
                print('Não existe médico com esse CRM.')

    @staticmethod
    def get_types(content):
        return [
            value.strftime("%d/%m/%Y")
            if isinstance(value, datetime.date)
            else str(value)
            for value in content
        ]

    def update_doctor(self):
        crm = input("CRM do médico: ")
        if self.get_doctor(crm):
            _, *options, _ = self.field_options
            print(f"As opções de campos são: {options}")
            while True:
                field = input('Campo para alterar: ')
                field = self.field_translation.get(field.upper().strip())
                if not field:
                    print('O campo preencido está incorreto, favo preencher novamente.')
                else:
                    break
            value = input('Novo valor: ')
            with self.database() as db:
                db.execute(f"UPDATE DOCTOR set {field.upper()} = '{value}' where CRM = '{crm}';")

    def list_doctors(self):
        with self.database() as db:
            template = "{0:15}|{1:15}|{2:15}|{3:15}|{4:15}"
            print(template.format(*self.field_options))
            print('-'*80)
            for doctor in db.query_all('DOCTOR'):
                print(template.format(*self.get_types(doctor)))

    def delete_doctor(self):
        crm = input("CRM do médico: ")
        if self.get_doctor(crm):
            with self.database() as db:
                db.execute(f"DELETE FROM DOCTOR WHERE CRM = '{crm}'")
                return f'Médico de CRM {crm} removido com sucesso!'

    def export_to_csv(self):
        with self.database() as db:
            with open('results_file.csv', 'w') as f:
                db.copy_expert('DOCTOR', f)
                print('\nDados exportados com sucesso!\n')

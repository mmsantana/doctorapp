from psycopg2._psycopg import ProgrammingError

from doctor_app.doctor_management import DoctorManagement


class Run:

    def __init__(self):
        doctor = DoctorManagement()
        try:
            doctor.create_table()
        except ProgrammingError:
            pass
        self.methods = {
            '1': doctor.get_doctor,
            '2': doctor.insert_doctor,
            '3': doctor.update_doctor,
            '4': doctor.delete_doctor,
            '5': doctor.list_doctors,
            '6': doctor.export_to_csv
        }

    def doctor_manipulation(self, method):
        method = self.methods.get(method)
        if method:
            return method()
        else:
            self.error_message()

    def execute(self):
        print('Bem vindo ao sistema de criação e acesso a pacientes e médicos.')
        while True:
            method = self.action()
            if method.strip() == '7':
                break
            self.doctor_manipulation(method=method)

    @staticmethod
    def action():
        print('\nEscolha a ação desejada digitando o número:')
        print('1) Consultar \t 2) Cadastrar \t 3) Alterar \t 4) Excluir \t 5) Listar \t 6) Exportar \t 7) Sair')
        return input("\nResposta: ").replace(')', '')

    @staticmethod
    def error_message():
        print('\nA opção não existe, tente novamente!\n')


if __name__ == '__main__':
    run = Run()
    run.execute()

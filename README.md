# DoctorApp

Essa aplicação tem o objetivo de implementar as funcionalidades CRUD para 
um sistema de acesso e modificação de dados em banco de dados.

## Operações do sistema
O sistema terá as seguintes operações:

### Getão dos usuários
O usuário poderá realizar as seguintes operações:

* Criação de médicos;

* Listagem dos médicos;

* Alteração dos dados;

* Deleção de médicos;

* Exportação de todos os dados da tabela para arquivo do formato .csv.
